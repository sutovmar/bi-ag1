#include <iostream>
#include <vector>


#define modul 1000000007

using namespace std;

//---------------------------

class WrongInputException {
	
};

//---------------------------

class Grid {

	private:
		unsigned int width, height;
		vector<int*> r_data;
		vector<int*> c_data;

	public:

		Grid(){
			int r_input = 0;
			int c_input = 0;

			cin >> this->width;
			cin >> this->height;
			
			cout << "Parsing r value" << endl;

			//parse r value
			for ( unsigned int i = 0; i < this->width; i++){
				

				cin >> r_input;

				if( cin.eof() )
					throw WrongInputException();	
				
				this->r_data.emplace_back(new int*( &r_input));
			}


			cout << "Parsing c value" << endl;
			//parse c value
		
			for( unsigned int j = 0; j < this->height; j++){
				
				int * input;
				
				cin >> c_input;

				if( cin.eof() )
					throw WrongInputException();
				
				(*input) = c_input;

				this->c_data.emplace_back(input);
			}

		}
		
		~Grid(){
			for( auto r : this->r_data) 
				delete r;

			this->r_data.clear();
			this->r_data.shrink_to_fit();

			for( auto c : this->c_data)
			       delete c;

			this->c_data.clear();
			this->c_data.shrink_to_fit();

		}

};

//---------------------------

int main()
{
	Grid test;

	return 0;	

}
