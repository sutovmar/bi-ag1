#include <iostream>
#include <cassert>
#include <string>
#include <cstring>
#include <map>
#include <list>
#include <set>
#include <queue>

using namespace std;

class LunchFront{

	private:

//================================================================

		int f_width;
		int f_stepsize;

		string front_l;
		string front_r;

		int easierBFS()
		{
			queue<int> opened;
			set<int> visited;
			map<int, int> length_map;
			set<int> cleaned;
			int min = -1;

			visited.emplace(0);
			opened.push(0);

			length_map.emplace(0,0);

			while( !opened.empty() )
			{
				int current = opened.front();
				int length = length_map[current];

				opened.pop();

				//right graph
				if ( current < this->f_width )
				{
					if ( this->front_r[current] != 'x' )
					{
						if ( (current + 1) >= this->f_width || ( current + this->f_stepsize ) >= this->f_width )
						{
							//find end
							if( (length + 1) < min || min == -1)
								min = length+1;

						} else {
							
							if ( (current+1) < this->f_width && this->front_r[current+1] != 'x' )
							{
								int node = current + 1;

								if ( visited.find(node) == visited.end()){

									if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){

										opened.push(node);
										length_map.emplace(node, length+1);
										visited.emplace(node);
									}
								}

							}	

							if( (current-1) >= 0 && this->front_r[current-1] != 'x' ){ 

								int node = current -1;

																if ( visited.find(node) == visited.end()){

								if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){

									opened.push(node);
									length_map.emplace(node, length+1);
									visited.emplace(node);

								}
							}

							}

							if( (current+this->f_stepsize < this->f_width) && (this->front_l[current+this->f_stepsize] != 'x' ) ){
								
								int node = current + this->f_width + this->f_stepsize;

																if ( visited.find(node) == visited.end()){

								if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){

									opened.push(node);
									length_map.emplace(node, length+1);
									visited.emplace(node);

								}
							}

							}
						}
					}
				} 
				//left graph
				else {

					current = current- this->f_width;

					if ( this->front_l[current] != 'x' )
					{
						if ( ( current + this->f_stepsize ) >= this->f_width || (current+1) >= this->f_width )
						{
							//find end
							if( (length + 1) < min || min == -1)
								min = length+1;

						} else {

								if ( (current+1) < this->f_width && this->front_l[current+1] != 'x' )
								{
									int node = current +1 + this->f_width;

																	if ( visited.find(node) == visited.end()){


									if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){
										opened.push(node);
										length_map.emplace(node, length+1);
										visited.emplace(node);
									}
								}
								}


								if( (current-1) >= 0 && this->front_l[current-1] != 'x' )
								{
									int node = current -1 + this->f_width;

																	if ( visited.find(node) == visited.end()){


									if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){
										opened.push(node);
										length_map.emplace(node, length+1);
										visited.emplace(node);
									}
									}
								}

								if( (current+this->f_stepsize < this->f_width) && (this->front_r[current+this->f_stepsize] != 'x' ) )
								{
									int node = current + this->f_stepsize;

									if ( visited.find(node) == visited.end()){

										if ( ((node >= this->f_width && (node - this->f_width) > (length) ) || ( node < this->f_width && node > length )) ){
											opened.push(node);
											length_map.emplace(node, length+1);
											visited.emplace(node);
										}
									}

								}
						}

					}
				}

			}


			return min;
		}

//================================================================


	public:
//================================================================
		// LunchFront(int width, int stepsize, string front_right, string front_left) : f_width(width), f_stepsize(stepsize), front_l(front_left), front_r(front_right)
		LunchFront()
		{
			cin >> this->f_width;
			cin >> this->f_stepsize;
			cin >> this->front_r;
			cin >> this->front_l;

			cout << this->findShortestPath() << endl;
			
		};

//----------------------------------------------------------------

		int findShortestPath()
		{

			return easierBFS();
		}
//================================================================
};

int main()
{
	LunchFront l;
/*
	// LunchFront g0(10,4,"x.x....x..", ".. xx.x...xx");

	// cout << g0.findShortestPath() << endl;

	LunchFront g1(5,2,"x...x",".xx.x");

	// g1.displayGraph();
	cout << g1.findShortestPath() << endl;

// 	assert(g1.findShortestPath() == 3);

// //================================================================

	LunchFront g2(10,4,".xx.x...xx","x.x....x..");

	cout << g2.findShortestPath() << endl;
// 	assert(g2.findShortestPath() == 4);

// //================================================================

	LunchFront g3(5,1,"..x..","..xx.");

// 	// g3.displayGraph();
	cout << g3.findShortestPath() << endl;
// 	// assert(g3.findShortestPath() == -1);

	// LunchFront g4(5,3,".xx.","x...");
	
	// cout << g4.findShortestPath() << endl;
*/

	/*
	19 2
	.xx....x.x..x.xxx..
	xx...xx...xxx...xxx
	12

	15 4
	.xxxxxx.x...xxx
	xxx...xxx.x.xxx
	5
	
	5 1
	.....
	.....
	*/


	return 0;
}