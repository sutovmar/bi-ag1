//===========================================================================

struct Node {

	int id;
	unsigned int revenue;

};

//===========================================================================

struct CString {

	unsigned int id;
	Node ** goods;
	unsigned int all_size;
	unsigned int used_size;

//---------------------------------------------------------------------------

	CString()
	{
		this->all_size 	= 1000;
		this->used_size = 1;
	}

//---------------------------------------------------------------------------

	bool HasSons( int j )
	{
		return ( (unsigned)(2*j) <= (this->used_size-1) || (unsigned)(2*j + 1) <= (this->used_size - 1));
	}

//---------------------------------------------------------------------------

	pair<Node*, int> GetMinSons( int j )
	{

		if ( (unsigned)(2*j + 1) > (this->used_size - 1) )
			return make_pair(this->goods[2*j], 2*j);

		Node * left = this->goods[2*j];

		Node * right = this->goods[2*j+1];

		if( left->revenue == right->revenue )
			return (left->id < right->id) ? ( make_pair(left, 2*j) ): (make_pair(right, 2*j+1));

		return (left->revenue < right->revenue) ? ( make_pair(left, 2*j) ) : (make_pair(right, 2*j+1));
	}


//---------------------------------------------------------------------------

	void Clear()
	{
		for( unsigned int i = 1; i < this->used_size; i++)
			delete this->goods[i];

		delete [] this->goods;

		this->goods = NULL;

		this->used_size = 0;
		this->all_size 	= 0;
	}

//---------------------------------------------------------------------------

	void SwapPoints(int i, int j)
	{
			swap(this->goods[i],this->goods[j]);
			//swap(*this->goods[i],*this->goods[j]);
	}


//---------------------------------------------------------------------------
	bool CleanAfterDelete( int j )
	{
		while ( this->HasSons(j) )
		{
			Node * parent = this->goods[j];
			
			pair<Node*, int> minimal_son = this->GetMinSons(j);

			if (parent->revenue < minimal_son.first->revenue )
				return true;
		
			this->SwapPoints(j, minimal_son.second);
			
			j = minimal_son.second;
		}

		return true;
	}

//---------------------------------------------------------------------------

	Node GetMin(){

		return *this->goods[1];
	}

//---------------------------------------------------------------------------
	void DelMin()
	{
		
		this->SwapPoints(1, this->used_size-1);
		
		delete this->goods[this->used_size-1];

		this->used_size--;
		
		this->CleanAfterDelete(1);
	}

//---------------------------------------------------------------------------

	bool FixPoint( int j )
	{
		while ( j != 1 )
		{
			int parent_pos = floor(j/2);

			Node * current = this->goods[j];
			Node * parent = this->goods[parent_pos];
			
			if ( parent->revenue < current->revenue || ( parent->revenue == current->revenue && parent->id < current->id ) )
				return true;

			this->SwapPoints(j, parent_pos);
			
			j = parent_pos;

		}

		return true;
	}

//---------------------------------------------------------------------------

	void Add(unsigned int id, int revenue)
	{
		Node * new_node = new Node;

		new_node->id 		= id;
		new_node->revenue 	= revenue;

		if ( this->used_size == 1 ) {
			
			this->goods = new Node*[this->all_size];

			this->goods[this->used_size] = new_node;
			this->used_size++;
		
		} else {
			
			if( this->used_size >= this->all_size)
				this->Resize();

			this->goods[this->used_size] = new_node;

			this->FixPoint(this->used_size);

			this->used_size++;
		}
	}

//---------------------------------------------------------------------------
	
	void Resize()
	{
		this->all_size = this->all_size * 2;
		
		Node ** tmp = new Node*[this->all_size];
		
		for( unsigned int i = 0; i < this->used_size; i++ )
			tmp[i] = this->goods[i];
		
		delete [] this->goods;

		this->goods = tmp;
	}

//---------------------------------------------------------------------------

	bool IsEmpty(){
		return (this->used_size <= 1);
	}
//---------------------------------------------------------------------------

	void Print()
	{
		for( unsigned int i = 1; i < this->used_size; i=i+1)
		{

			Node * parent = this->goods[i];
			cout << parent->id << "(" << parent->revenue << ") " ;

	   	}

	    cout << endl;
	}


//---------------------------------------------------------------------------

};