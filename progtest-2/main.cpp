#include <iostream>
#include <string>
#include <cassert>
#include <math.h>
#include <climits>

using namespace std;

//===========================================================================

struct Node{

	int id;
	unsigned int revenue;

};

//===========================================================================

struct CString{

	int id;
	Node * goods;
	unsigned int all_size;
	unsigned int used_size;

	//------------------------------------------------------------------

	CString(){

		this->all_size  = 3;

		this->used_size = 1;
	}
	
	//-----------------------------------------------------------------

	void Clear(){

		delete [] this->goods;

		this->goods = NULL;

		this->used_size = 0;
		this->all_size 	= 0;
	}

	//-----------------------------------------------------------------
	
	Node GetMin(){

		return this->goods[1];
	}
//-----------------------------------------------------------------------
	void DelMin()
	{
		this->SwapPoints(1, this->used_size-1);
		this->used_size--;
		this->CleanAfterDelete(1);
	}


	//-----------------------------------------------------------------
	void SwapPoints(int i, int j)
	{
		Node tmp = this->goods[i];
		this->goods[i] = this->goods[j];
		this->goods[j] = tmp;
	}

	//-----------------------------------------------------------------

	bool FixPoint( int j )
	{
		while ( j != 1 )
		{
			int parent_pos = floor(j/2);

			Node current = this->goods[j];
			Node parent = this->goods[parent_pos];
			
			if ( parent.revenue < current.revenue || ( parent.revenue == current.revenue && parent.id < current.id ) )
				return true;

			this->goods[j] = parent;
			this->goods[parent_pos] = current;
			
			j = parent_pos;

		}

		return true;
	}

	//-----------------------------------------------------------------

	bool CleanAfterDelete( int j )
	{
		while ( (unsigned)(2*j) <= (this->used_size - 1) || (unsigned)(2*j + 1) <= (this->used_size - 1) )
		{
			Node parent = this->goods[j];
			
			Node min;
			int min_pos = 0;
			unsigned int min_rev = INT_MAX;

			if ( (unsigned)(2*j) <= (this->used_size - 1 ) )  
			{
				if ( this->goods[2*j].revenue < min_rev)
				{
					min = this->goods[2*j];
					min_pos = 2*j;
					min_rev = min.revenue;
				}	
			}

			if ( (unsigned)(2*j + 1) <= (this->used_size - 1) )
			{

				if ( this->goods[2*j+1].revenue < min_rev || (min_rev == this->goods[2*j+1].revenue && min.id > this->goods[2*j+1].id))
				{
					min = this->goods[2*j+1];
					min_pos = 2*j+1;
					min_rev = min.revenue;
				}	
			}

			if (parent.revenue < min.revenue )
				return true;

			this->goods[j] = min;
			this->goods[min_pos] = parent;

			j = min_pos;
		}

		return true;
	}

	//-----------------------------------------------------------------

	void Add(unsigned int id, int revenue)
	{
		Node new_node;

		new_node.id = id;
		new_node.revenue = revenue;

		if ( this->used_size == 1 ) {
			
			this->goods = new Node[this->all_size];

			this->goods[this->used_size] = new_node;
			this->used_size++;
		
		} else {
			
			if( this->used_size >= this->all_size)
				this->Resize();

			this->goods[this->used_size] = new_node;

			this->FixPoint(this->used_size);

			this->used_size++;
		}
	}	

	//------------------------------------------------------------------
	
	void Resize()
	{
		this->all_size = this->all_size * 2;
		
		Node * tmp = new Node[this->all_size];
		
		for( unsigned int i = 0; i < this->used_size; i++ )
			tmp[i] = this->goods[i];
		
		delete [] this->goods;

		this->goods = tmp;
	}

	//-----------------------------------------------------------------
	bool IsEmpty(){
		return (this->used_size <= 1);
	}
	//------------------------------------------------------------------

	void Print()
	       {
	               for( unsigned int i = 1; i < this->used_size; i=i+1 )
	               {
	                       Node parent = this->goods[i];

	                       cout << parent.id << "(" << parent.revenue << ") " ;

	               }

	               cout << endl;
	       }

	};

//===========================================================================

class CHolding
{
  public:
   
  //-------------------------------------------------------------------------

   CHolding(){

   	this->all_size = 100000;

	this->used_size = 0;

	this->chain_string = new CString[this->all_size];
   }
   
   //------------------------------------------------------------------------

   ~CHolding(){

   	for( unsigned int i = 0; i < this->used_size; i++ )
   		this->chain_string[i].Clear();

   	delete [] this->chain_string;

   	this->chain_string = NULL;
   }

   //------------------------------------------------------------------------

   void Add    (int chain, unsigned int id, unsigned int revenue)
   {
  	if ( !this->ChainExists(chain) ){

		CString new_chain;

		new_chain.id = chain;

		new_chain.Add(id, revenue);
		
		if(this->used_size >= this->all_size)
			this->Resize();

		this->chain_string[this->used_size] = new_chain;

		this->used_size = this->used_size + 1;

	} else {

		CString * goods_chain = this->GetChain(chain);
		
		goods_chain->Add(id, revenue);
		
	}

   };
   
   //------------------------------------------------------------------------
   
   bool Remove (int chain, unsigned int & id)
   {
   	if ( !this->ChainExists(chain) )
	{	
		id = 0;
		return false;
	}
	
	CString * goods_chain = this->GetChain(chain);
	
	if( goods_chain->IsEmpty()) 
	{
		id = 0;
		return false;
	}

	id = goods_chain->GetMin().id;
	
	goods_chain->DelMin();

	return true;
   };

   //------------------------------------------------------------------------

   bool Remove (unsigned int & id)
   {
   	
   	CString * min_chain = NULL;
   	unsigned min_rev = INT_MAX;	
	
	for( unsigned int i = 0; i < this->used_size; i++ )
	{	
		if ( !this->chain_string[i].IsEmpty() )
		{
			if ( ( this->chain_string[i].GetMin().revenue < min_rev ) || ( this->chain_string[i].GetMin().revenue == min_rev && this->chain_string[i].id < min_chain->id )   )
			{
				min_chain = &(this->chain_string[i]);

				min_rev = min_chain->GetMin().revenue;
				id = min_chain->GetMin().id;
			}
		}	
	}
	
	if ( min_chain == NULL ) {
		id = 0;
		return false;
	}

	min_chain->DelMin();

	return true;
   };

   //------------------------------------------------------------------------

   void Merge  (int dstChain, int srcChain)
   {
	
	CString * source;
    CString * destination;	

   	for ( unsigned int j = 0; j < this->used_size; j++){
		
		if ( this->chain_string[j].id == dstChain)
			destination = &(this->chain_string[j]);

		if( this->chain_string[j].id == srcChain )
			source = &(this->chain_string[j]);
	}
	
	for( unsigned int i = 1; i < source->used_size; i++)
	{
		destination->Add(source->goods[i].id, source->goods[i].revenue);
	}

	source->Clear();

   };

   //------------------------------------------------------------------------

   bool Merge  (void)
   {

   	//find first min
	unsigned int f_min = INT_MAX;
	int id_source;

	for( unsigned int i = 0; i < this->used_size; i++)
	{
		if( !this->chain_string[i].IsEmpty() && this->chain_string[i].GetMin().revenue < f_min)
		{
			f_min = this->chain_string[i].GetMin().revenue;
			id_source = this->chain_string[i].id;
		}
	}
   	
	if ( f_min == INT_MAX) 
		return false;

	//find second
	f_min = INT_MAX;
	int id_dest;

	for( unsigned int i = 0; i < this->used_size; i++ )
	{
		if ( !this->chain_string[i].IsEmpty() && this->chain_string[i].GetMin().revenue < f_min && this->chain_string[i].id != id_source)
		{
			f_min = this->chain_string[i].GetMin().revenue;
			id_dest = this->chain_string[i].id;
		}
	}

	if ( f_min == INT_MAX) 
		return false;

	if ( id_dest < id_source)
		this->Merge(id_dest, id_source);
	else 
		this->Merge(id_source, id_dest);

	return true;

   };

   void Print(){
     
          for( unsigned int i = 0; i < this->used_size; i++)
          {
                  cout << "Chain number : " << this->chain_string[i].id << endl;
                  this->chain_string[i].Print();

                  cout << "=================================================" << endl;

          }

     }

 
  private:

   CString * chain_string;
   
   unsigned int all_size;

   unsigned int used_size;

   //------------------------------------------------------------------------
   
   CString *  GetChain( int chain )
   {
   		for( unsigned int i = 0; i < this->used_size; i++ )
   		{
   			if ( this->chain_string[i].id == chain )
   			{
   				return &this->chain_string[i];
   			}
   		}

		return nullptr;
   }

   //------------------------------------------------------------------------

   bool ChainExists ( int chain )
   {
   		if ( this->used_size == 0 )
   			return false;

   		for( unsigned int i = 0; i < this->used_size; i++ ){

   			if ( this->chain_string[i].id == chain)
   				return true;
   		}

   		return false;
   }

   //------------------------------------------------------------------------
   
   void Resize()
   {
	   this->all_size = this->all_size*2;

	   CString * tmp = new CString[this->all_size];

	   for( unsigned int i = 0; i < this->used_size; i++ )
	   	tmp[i] = this->chain_string[i];

	   delete [] this->chain_string;

	   this->chain_string = tmp;
   }
};

//===========================================================================


int main()
{
	// bool res;
	// unsigned int id;

	// // //=============================================================

	// CHolding f1;
	
	// f1 . Add ( 7, 2, 9 );
	// f1 . Add ( 12, 4, 4 );
	// f1 . Add ( 6, 15, 2 );
	// f1 . Add ( 6, 9, 3 );


	// res = f1 . Remove ( 12, id );
	// assert(res == true && id == 4);
	
	// res = f1 . Remove ( 12, id );
 // 	assert(res == false && id == NULL);

 // 	// f1.Print();
	
	// res = f1 . Remove ( 6, id );
 // 	assert(res == true && id == 15);

	// res = f1 . Remove ( 6, id );
	// assert(res == true && id == 9);

	// res = f1 . Remove ( 6, id );
 // 	assert(res == false && id == NULL);

 // 	//=============================================================

	// CHolding f2;
	// f2 . Add ( 4, 2, 2 );
	// f2 . Add ( 1, 4, 3 );
	// f2 . Add ( 8, 9, 8 );

	// res = f2 . Remove ( id );
	// assert(res == true && id == 2);

	// res = f2 . Remove ( id );
	// assert(res == true && id == 4);

	// //=============================================================
	
	// CHolding f3;
	// f3 . Add ( 10, 101, 9 );
	// f3 . Add ( 10, 102, 8 );
	// f3 . Add ( 10, 103, 7 );
	// f3 . Add ( 10, 104, 6 );
	// f3 . Add ( 10, 105, 5 );
	// f3 . Add ( 20, 201, 9 );
	// f3 . Add ( 20, 202, 8 );
	// f3 . Add ( 20, 203, 7 );
	// f3 . Add ( 20, 204, 6 );
	// f3 . Add ( 20, 205, 5 );
	// f3 . Add ( 30, 301, 9 );
	// f3 . Add ( 30, 302, 8 );
	// f3 . Add ( 30, 303, 7 );
	// f3 . Add ( 30, 304, 6 );
	// f3 . Add ( 30, 305, 5 );

	// res = f3 . Remove ( id );
	// assert(res == true && id == 105);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 205);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 305);
	
	// res = f3 . Remove ( id );
	// assert(res == true && id == 104);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 204);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 304);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 103);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 203);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 303);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 102);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 202);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 302);

	// //=============================================================

	// CHolding f4;
	// f4 . Add ( 10, 101, 9 );
	// f4 . Add ( 10, 102, 8 );
	// f4 . Add ( 10, 103, 7 );
	// f4 . Add ( 10, 104, 6 );
	// f4 . Add ( 10, 105, 5 );
	// f4 . Add ( 20, 201, 9 );
	// f4 . Add ( 20, 202, 8 );
	// f4 . Add ( 20, 203, 7 );
	// f4 . Add ( 20, 204, 6 );
	// f4 . Add ( 20, 205, 5 );
	// f4 . Add ( 30, 301, 9 );
	// f4 . Add ( 30, 302, 8 );
	// f4 . Add ( 30, 303, 7 );
	// f4 . Add ( 30, 304, 6 );
	// f4 . Add ( 30, 305, 5 );

	
	// res = f4 . Remove ( 30, id );
	// assert(res == true && id == 305);
	
	// res = f4 . Remove ( 20, id );
	// assert(res == true && id == 205);

	// res = f4 . Remove ( 10, id );
	// assert(res == true && id == 105);

	// f4 . Merge ( 30, 10 );

	// res = f4 . Remove ( 10, id );
	// assert( res == false && id == NULL);

	// res = f4 . Remove ( 20, id );
	// assert(res == true && id == 204);

	// res = f4 . Remove ( 30, id );
	// assert(res == true && id == 104);

	// res = f4 . Remove ( id );
	// assert(res == true && id == 304);
	
	// res = f4 . Remove ( id );
	// assert(res == true && id == 203);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 103);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 303);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 202);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 102);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 302);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 201);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 101);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 301);
	
	// res = f4 . Remove ( id );
	// assert( res == false && id == NULL);

	// res = f4 . Remove ( id );
	// assert( res == false && id == NULL);

	// //=============================================================

	// CHolding f5;
	// f5 . Add ( 10, 333, 5 );
	// f5 . Add ( 20, 444, 2 );
	// f5 . Add ( 10, 222, 6 );
	// f5 . Add ( 20, 555, 8 );

	// res = f5 . Remove ( 10, id );
	// assert( res == true && id == 333);

	// res = f5 . Remove ( id );
	// assert( res == true && id == 444);

	// f5 . Merge ( 20, 10 );

	// res = f5 . Remove ( 10, id );
	// assert(res == false && id == NULL);

	// res = f5 . Remove ( 20, id );
	// assert( res == true && id == 222);

	// //=============================================================

	// CHolding f6;
	// f6 . Add ( 10, 1, 7 );
	// f6 . Add ( 20, 1, 7 );
	// f6 . Add ( 30, 1, 7 );

	// res = f6 . Merge ( );
	// assert( res == true);

	// res = f6 . Remove ( 20, id );
	// assert ( res == false && id == NULL);

	// res = f6 . Remove ( 30, id );
	// assert( res == true && id == 1);

	// res = f6 . Remove ( 30, id );
	// assert( res == false && id == NULL);

	// res = f6 . Remove ( 10, id );
	// assert ( res == true && id == 1);

	// res = f6 . Remove ( 10, id );
	// assert( res == true && id == 1);
	
	// res = f6 . Remove ( 10, id );
	// assert(res == false && id == NULL);

	// //=============================================================

	// //Ukazkovy vstup #7
	// //-----------------
	// CHolding f7;
	// f7 . Add ( 1, 1, 1 );
	// f7 . Add ( 2, 2, 1 );
	// f7 . Add ( 3, 3, 1 );

	// res = f7 . Merge ( );
	// assert( res == true);
	
	// res = f7 . Merge ( );
	// assert( res == true);

	// res = f7 . Merge ( );
	// assert( res == false);
	
	// res = f7 . Remove ( id );
	// assert( res == true &&  id == 1 );

	// res = f7 . Remove ( id );
	// assert( res == true && id == 2);

	// res = f7 . Remove ( id );
	// assert( res == true && id == 3);

	// res = f7 . Remove ( id );
	// assert( res == false && id == NULL);

	// //=============================================================

	return 0;
}
