#include <iostream>
#include <string>
#include <cassert>
#include <math.h>
#include <climits>

using namespace std;

//===========================================================================

struct Node {

	int id;
	unsigned int revenue;

};

//===========================================================================

struct CString {

	unsigned int id;
	Node ** goods;
	unsigned int all_size;
	unsigned int used_size;

//---------------------------------------------------------------------------

	CString()
	{
		this->all_size 	= 1000;
		this->used_size = 1;
	}

//---------------------------------------------------------------------------

	bool HasSons( int j )
	{
		return ( (unsigned)(2*j) <= (this->used_size-1) || (unsigned)(2*j + 1) <= (this->used_size - 1));
	}

//---------------------------------------------------------------------------

	pair<Node*, int> GetMinSons( int j )
	{

		if ( (unsigned)(2*j + 1) > (this->used_size - 1) )
			return make_pair(this->goods[2*j], 2*j);

		Node * left = this->goods[2*j];

		Node * right = this->goods[2*j+1];

		if( left->revenue == right->revenue )
			return (left->id < right->id) ? ( make_pair(left, 2*j) ): (make_pair(right, 2*j+1));

		return (left->revenue < right->revenue) ? ( make_pair(left, 2*j) ) : (make_pair(right, 2*j+1));
	}


//---------------------------------------------------------------------------

	void Clear()
	{
		for( unsigned int i = 1; i < this->used_size; i++)
			delete this->goods[i];

		delete [] this->goods;

		this->goods = NULL;

		this->used_size = 0;
		this->all_size 	= 0;
	}

//---------------------------------------------------------------------------

	void SwapPoints(int i, int j)
	{
			swap(this->goods[i],this->goods[j]);
			//swap(*this->goods[i],*this->goods[j]);
	}


//---------------------------------------------------------------------------
	bool CleanAfterDelete( int j )
	{
		while ( this->HasSons(j) )
		{
			Node * parent = this->goods[j];
			
			pair<Node*, int> minimal_son = this->GetMinSons(j);

			if (parent->revenue < minimal_son.first->revenue )
				return true;
		
			this->SwapPoints(j, minimal_son.second);
			
			j = minimal_son.second;
		}

		return true;
	}

//---------------------------------------------------------------------------

	Node GetMin(){

		return *this->goods[1];
	}

//---------------------------------------------------------------------------
	void DelMin()
	{
		
		this->SwapPoints(1, this->used_size-1);
		
		delete this->goods[this->used_size-1];

		this->used_size--;
		
		this->CleanAfterDelete(1);
	}

//---------------------------------------------------------------------------

	bool FixPoint( int j )
	{
		while ( j != 1 )
		{
			int parent_pos = floor(j/2);

			Node * current = this->goods[j];
			Node * parent = this->goods[parent_pos];
			
			if ( parent->revenue < current->revenue || ( parent->revenue == current->revenue && parent->id < current->id ) )
				return true;

			this->SwapPoints(j, parent_pos);
			
			j = parent_pos;

		}

		return true;
	}

//---------------------------------------------------------------------------

	void Add(unsigned int id, int revenue)
	{
		Node * new_node = new Node;

		new_node->id 		= id;
		new_node->revenue 	= revenue;

		if ( this->used_size == 1 ) {
			
			this->goods = new Node*[this->all_size];

			this->goods[this->used_size] = new_node;
			this->used_size++;
		
		} else {
			
			if( this->used_size >= this->all_size)
				this->Resize();

			this->goods[this->used_size] = new_node;

			this->FixPoint(this->used_size);

			this->used_size++;
		}
	}

//---------------------------------------------------------------------------
	
	void Resize()
	{
		this->all_size = this->all_size * 2;
		
		Node ** tmp = new Node*[this->all_size];
		
		for( unsigned int i = 0; i < this->used_size; i++ )
			tmp[i] = this->goods[i];
		
		delete [] this->goods;

		this->goods = tmp;
	}

//---------------------------------------------------------------------------

	bool IsEmpty(){
		return (this->used_size <= 1);
	}
//---------------------------------------------------------------------------

	void Print()
	{
		for( unsigned int i = 1; i < this->used_size; i=i+1)
		{

			Node * parent = this->goods[i];
			cout << parent->id << "(" << parent->revenue << ") " ;

	   	}

	    cout << endl;
	}


//---------------------------------------------------------------------------

};

//===========================================================================

class CHolding
{
  public:
   
//---------------------------------------------------------------------------

  	CHolding()
  	{

  		this->all_size = 10000;

		this->used_size = 1;

		this->chain_string = new CString*[this->all_size];

  	}

//---------------------------------------------------------------------------

  	~CHolding()
  	{

  		for( unsigned int i = 1; i < this->used_size; i++ )
   		{
   			this->chain_string[i]->Clear();
   			delete this->chain_string[i];
   		}

   		delete [] this->chain_string;

   		this->chain_string = NULL;

  	}

//---------------------------------------------------------------------------
  
   void Add    (int chain, unsigned int id, unsigned int revenue)
   {
   		int chain_position = this->GetChain(chain);

   		if( chain_position == -1 )
   			chain_position = this->CreateChain(chain);

   		if ( this->used_size >= 2 )
   		{
   		
   			if ( this->chain_string[chain_position]->IsEmpty() )
   			{
   				this->chain_string[chain_position]->Add(id, revenue);

   				this->BubbleUp(chain_position);
   			}
   			else if ( revenue < this->chain_string[chain_position]->GetMin().revenue )
   			{
   				this->chain_string[chain_position]->Add(id, revenue);
   				this->BubbleUp(chain_position);

   			} else{

   				this->chain_string[chain_position]->Add(id, revenue);
   				this->BubbleDown(chain_position);
   			} 

   		} else {

   			this->chain_string[chain_position]->Add(id, revenue);
   		}

   };
//---------------------------------------------------------------------------

   bool Remove (int chain, unsigned int & id)
   {
   		id = 0;

   		int chain_position = this->GetChain(chain);

   		if ( chain_position == -1 || this->chain_string[chain_position]->IsEmpty() )
			return false;
	
		CString * goods_chain = this->chain_string[chain_position];

		id = goods_chain->GetMin().id;

		unsigned int rev = goods_chain->GetMin().revenue;

		goods_chain->DelMin();

		if ( goods_chain->IsEmpty() )
		{
			this->Repair(chain_position);

			this->BubbleDown(chain_position);

			return true;
		}

		if( rev < goods_chain->GetMin().revenue )
			this->BubbleDown(chain_position);
		else
			this->BubbleUp(chain_position);

		return true;
   };

//---------------------------------------------------------------------------
   bool Remove (unsigned int & id)
   {
   		if ( this->used_size <= 1 )
   		{
   			id = 0;
   			return false;
   		}

		return this->Remove(this->chain_string[1]->id, id);
   };

//---------------------------------------------------------------------------
   void Merge  (int dstChain, int srcChain)
   {	
   		int source_position;
   		int destination_position;
   		
   		source_position  		= this->GetChain(srcChain);
   		destination_position 	= this->GetChain(dstChain);

   		if ( source_position != -1 && destination_position != -1 ) {

	   		CString * source 		= this->chain_string[source_position];
	    	CString * destination	= this->chain_string[destination_position];


	    	for( unsigned int i = 1; i < source->used_size; i++)
			{
				destination->Add(source->goods[i]->id, source->goods[i]->revenue);
			}

			source->Clear();

			this->Repair(source_position);
		}
   };

//---------------------------------------------------------------------------
   bool Merge  (void)
   {
   		if ( this->used_size <= 1 )
   			return false;

   		if ( this->HasSons(1) )
   		{
   			CString * parent = this->chain_string[1];
   			CString * min_son = this->GetMinSons(1).first;

   			if ( parent->id < min_son->id )
   			{
   				this->Merge(parent->id, min_son->id);
   			} else {

   				this->Merge( min_son->id, parent->id);
   			}

   			return true;
   		}

   		return false;

   };

//---------------------------------------------------------------------------

    void Print(){
     
          for( unsigned int i = 1; i < this->used_size; i++)
          {
                  cout << "Chain number : " << this->chain_string[i]->id << endl;
                  this->chain_string[i]->Print();

                  cout << "=================================================" << endl;

          }

     }

//---------------------------------------------------------------------------

  private:
    
   CString ** chain_string;
   
   unsigned int all_size;

   unsigned int used_size;

//---------------------------------------------------------------------------

   int CreateChain( unsigned int chain_id )
   {
   		CString * new_chain = new CString;

   		new_chain->id = chain_id;

   		if ( this->used_size >= this->all_size)
   			this->Resize();
	
		this->chain_string[this->used_size] = new_chain;
		
		this->used_size++;

   		return this->used_size-1; 
   }

//---------------------------------------------------------------------------

   int GetChain( unsigned int chain_id )
   {

   	if( this->used_size == 1  )
   		return -1;
	
	for( unsigned int i = 1; i < this->used_size; i++)
	{
		if ( this->chain_string[i]->id == chain_id)
			return i;
	}	
		  
 	return -1; 
   }

//---------------------------------------------------------------------------

   void Resize()
   {
   	  this->all_size = this->all_size*2;

	   CString **  tmp = new CString*[this->all_size];

	   for( unsigned int i = 0; i < this->used_size; i++ )
	   		tmp[i] = this->chain_string[i];

	   delete [] this->chain_string;

	   this->chain_string = tmp;
   }

//---------------------------------------------------------------------------

  bool BubbleUp( int j )
  {

  	while ( j != 1 )
  	{
  		int parent_position = floor(j/2);

  		CString * current = this->chain_string[j];
		CString * parent = this->chain_string[parent_position];

		if ( parent->GetMin().revenue < current->GetMin().revenue || ( parent->GetMin().revenue == current->GetMin().revenue && parent->id < current->id ) )
			return true;
	
		swap(this->chain_string[j], this->chain_string[parent_position]);

		j  = parent_position;
  	}

  	return true;
  }

//---------------------------------------------------------------------------

  bool BubbleDown( int j )
  {
  	
  		while ( this->HasSons(j) )
  		{
			CString * parent = this->chain_string[j];

  			pair<CString*, int> min_son = this->GetMinSons(j);

  			if (parent->GetMin().revenue < min_son.first->GetMin().revenue || ( parent->GetMin().revenue == min_son.first->GetMin().revenue && parent->id < min_son.first->id )  )
				return true;
		
			swap(this->chain_string[j], this->chain_string[min_son.second]);
			
			j = min_son.second;

  		}

  		return true;
  }

//---------------------------------------------------------------------------

  void Repair( int j )
  {
  		swap(this->chain_string[j], this->chain_string[this->used_size-1]);

  		this->chain_string[this->used_size-1]->Clear();

  		delete this->chain_string[this->used_size-1];
  		
  		this->used_size--;
}


//---------------------------------------------------------------------------

  bool HasSons(int j)
  {
  	return ((unsigned)(2*j) <= (this->used_size - 1) || (unsigned)(2*j + 1) <= (this->used_size - 1));
  };

//---------------------------------------------------------------------------

  pair<CString*, int> GetMinSons( int j )
  {
  	if ( (unsigned)(2*j + 1) > (this->used_size - 1) )
			return make_pair(this->chain_string[2*j], 2*j);

		CString * left = this->chain_string[2*j];

		CString * right = this->chain_string[2*j+1];

		if( left->GetMin().revenue == right->GetMin().revenue )
			return (left->id < right->id) ? ( make_pair(left, 2*j) ): (make_pair(right, 2*j+1));

		return (left->GetMin().revenue < right->GetMin().revenue) ? ( make_pair(left, 2*j) ) : (make_pair(right, 2*j+1));
  };

//---------------------------------------------------------------------------

};

//===========================================================================

int main()
{

	// bool res;
	// unsigned int id;

	// //=============================================================

	// CHolding f1;
	
	// f1 . Add ( 7, 2, 9 );
	// f1 . Add ( 12, 4, 4 );
	// f1 . Add ( 6, 15, 2 );
	// f1 . Add ( 6, 9, 3 );

	// // f1.Print();

	// res = f1 . Remove ( 12, id );
	// assert(res == true && id == 4);

	// res = f1 . Remove ( 12, id );
 // 	assert(res == false && id == NULL);

	// res = f1 . Remove ( 6, id );
 // 	assert(res == true && id == 15);

	// res = f1 . Remove ( 6, id );
	// assert(res == true && id == 9);

	// res = f1 . Remove ( 6, id );
 // 	assert(res == false && id == NULL);

 // 	//=============================================================

	// CHolding f2;
	// f2 . Add ( 4, 2, 2 );
	// f2 . Add ( 1, 4, 3 );
	// f2 . Add ( 8, 9, 8 );
	
	// res = f2 . Remove ( id );
	// assert(res == true && id == 2);
	
	// res = f2 . Remove ( id );
	// assert(res == true && id == 4);

	// //=============================================================

	// CHolding f3;
	// f3 . Add ( 10, 101, 9 );
	// f3 . Add ( 10, 102, 8 );
	// f3 . Add ( 10, 103, 7 );
	// f3 . Add ( 10, 104, 6 );
	// f3 . Add ( 10, 105, 5 );
	// f3 . Add ( 20, 201, 9 );
	// f3 . Add ( 20, 202, 8 );
	// f3 . Add ( 20, 203, 7 );
	// f3 . Add ( 20, 204, 6 );
	// f3 . Add ( 20, 205, 5 );
	// f3 . Add ( 30, 301, 9 );
	// f3 . Add ( 30, 302, 8 );
	// f3 . Add ( 30, 303, 7 );
	// f3 . Add ( 30, 304, 6 );
	// f3 . Add ( 30, 305, 5 );

	// res = f3 . Remove ( id );
	// assert(res == true && id == 105);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 205);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 305);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 104);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 204);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 304);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 103);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 203);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 303);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 102);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 202);

	// res = f3 . Remove ( id );
	// assert(res == true && id == 302);

	// //=============================================================

	// CHolding f4;
	// f4 . Add ( 10, 101, 9 );
	// f4 . Add ( 10, 102, 8 );
	// f4 . Add ( 10, 103, 7 );
	// f4 . Add ( 10, 104, 6 );
	// f4 . Add ( 10, 105, 5 );
	// f4 . Add ( 20, 201, 9 );
	// f4 . Add ( 20, 202, 8 );
	// f4 . Add ( 20, 203, 7 );
	// f4 . Add ( 20, 204, 6 );
	// f4 . Add ( 20, 205, 5 );
	// f4 . Add ( 30, 301, 9 );
	// f4 . Add ( 30, 302, 8 );
	// f4 . Add ( 30, 303, 7 );
	// f4 . Add ( 30, 304, 6 );
	// f4 . Add ( 30, 305, 5 );

	
	// res = f4 . Remove ( 30, id );
	// assert(res == true && id == 305);

	// res = f4 . Remove ( 20, id );
	// assert(res == true && id == 205);

	// res = f4 . Remove ( 10, id );
	// assert(res == true && id == 105);

	// f4 . Merge ( 30, 10 );

	// res = f4 . Remove ( 10, id );
	// assert( res == false && id == NULL);

	// res = f4 . Remove ( 20, id );
	// assert(res == true && id == 204);

	// res = f4 . Remove ( 30, id );
	// assert(res == true && id == 104);

	// res = f4 . Remove ( id );
	// assert(res == true && id == 304);
	
	// res = f4 . Remove ( id );
	// assert(res == true && id == 203);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 103);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 303);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 202);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 102);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 302);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 201);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 101);

	// res = f4 . Remove ( id );
	// assert( res == true && id == 301);

	// res = f4 . Remove ( id );
	// assert( res == false && id == NULL);

	// res = f4 . Remove ( id );
	// assert( res == false && id == NULL);

	// //=============================================================

	// CHolding f5;
	// f5 . Add ( 10, 333, 5 );
	// f5 . Add ( 20, 444, 2 );
	// f5 . Add ( 10, 222, 6 );
	// f5 . Add ( 20, 555, 8 );

	// res = f5 . Remove ( 10, id );
	// assert( res == true && id == 333);

	// res = f5 . Remove ( id );
	// assert( res == true && id == 444);

	// f5 . Merge ( 20, 10 );

	// res = f5 . Remove ( 10, id );
	// assert(res == false && id == NULL);

	// res = f5 . Remove ( 20, id );
	// assert( res == true && id == 222);

	// //=============================================================

	// CHolding f6;
	// f6 . Add ( 10, 1, 7 );
	// f6 . Add ( 20, 1, 7 );
	// f6 . Add ( 30, 1, 7 );

	// res = f6 . Merge ( );
	// assert( res == true );

	// res = f6 . Remove ( 20, id );
	// assert ( res == false && id == NULL);

	// res = f6 . Remove ( 30, id );
	// assert( res == true && id == 1);

	// res = f6 . Remove ( 30, id );
	// assert( res == false && id == NULL);

	// res = f6 . Remove ( 10, id );
	// assert ( res == true && id == 1);

	// res = f6 . Remove ( 10, id );
	// assert( res == true && id == 1);

	// res = f6 . Remove ( 10, id );
	// assert(res == false && id == NULL);

	// //=============================================================

	// CHolding f7;
	// f7 . Add ( 1, 1, 1 );
	// f7 . Add ( 2, 2, 1 );
	// f7 . Add ( 3, 3, 1 );

	// res = f7 . Merge ( );
	// assert( res == true);

	// res = f7 . Merge ( );
	// assert( res == true);

	// res = f7 . Merge ( );
	// assert( res == false);

	// res = f7 . Remove ( id );
	// assert( res == true &&  id == 1 );

	// res = f7 . Remove ( id );
	// assert( res == true && id == 2);

	// res = f7 . Remove ( id );
	// assert( res == true && id == 3);

	// res = f7 . Remove ( id );
	// assert( res == false && id == NULL);

	// //==============================================================

	// CHolding f8;
	// f8 . Add ( 10, 101, 9 );
	// f8 . Add ( 10, 102, 8 );
	// f8 . Add ( 10, 103, 7 );
	// f8 . Add ( 10, 104, 6 );
	// f8 . Add ( 20, 105, 5 );

	// f8 . Add ( 20, 201, 9 );
	// f8. Remove( 20, id);
	// // f8.Print();
	// f8 . Add ( 20, 105, 5 );
	
	// f8.Remove(id);
	// f8 . Add ( 20, 202, 8 );
	// f8 . Add ( 20, 203, 7 );
	// f8 . Add ( 20, 204, 6 );

	// f8 . Add ( 10, 205, 5 );
	// f8 . Add ( 30, 301, 9 );
	// f8 . Add ( 30, 302, 8 );
	// f8 . Add ( 30, 303, 7 );
	// f8 . Add ( 30, 304, 6 );
	// f8 . Add ( 30, 305, 5 );

	// CHolding c2;
	// c2.Add(30, 80, 4);
	// c2.Add(30, 83, 3);
	// c2.Add(30, 82, 2);
	// c2.Add(30, 81, 1);
	// c2.Add(30, 181, 1);
	// c2.Add(20, 90, 4);
	// c2.Add(20, 93, 3);
	// c2.Add(20, 92, 2);
	// c2.Add(20, 291, 1);
	// c2.Add(20, 191, 1);

	// // c2.Print();
	// //--- 
	// res = c2.Remove(id);
	// assert(res && id == 191);
	// res = c2.Remove(id);
	// assert(res && id == 291);
	// res = c2.Remove(id);
	// assert(res && id == 81);
	// res = c2.Remove(id);
	// assert(res && id == 181);
	// res = c2.Remove(id);
	// assert(res && id == 92);
	// res = c2.Remove(id);
	// assert(res && id == 82);
	// res = c2.Remove(id);
	// assert(res && id == 93);
	// res = c2.Remove(id);
	// assert(res && id == 83);
	// res = c2.Remove(id);
	// assert(res && id == 90);
	// res = c2.Remove(id);
	// assert(res && id == 80);
	// res = c2.Remove(id);
	// assert(!res && id == NULL);

	return 0;
}
